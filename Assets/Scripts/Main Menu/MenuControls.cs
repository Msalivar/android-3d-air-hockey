﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuControls : MonoBehaviour
{
    public enum lastScore
    {
        player,
        cpu
    }

	static public Vector3 lastMousePosition = new Vector3(0, 2.58f, -2.751f);
    static public Vector3 p2LastMousePosition = new Vector3(0, 2.58f, 2.751f);
    Camera mainCamera;
    GameObject menu, overlay, score, paused, gameOver, playerSelect, diffSelect;
    static public bool pauseGame, twoPlayerMode;
    static public int playerScore, cpuScore;
    static public lastScore last = lastScore.cpu;
    const int scoreToWin = 6;
    Button pButton;
    int p1Touch = -1;
    int p2Touch = -1;
    AudioSource menuAudio;
    AudioClip menuSound;
    AudioClip gameStartSound;
    AudioClip gameOverSound;

    void Start()
	{
		Screen.SetResolution (720, 1280, true);
		if (GetComponent<Camera>().name == "Main Camera")
		{
			mainCamera = GetComponent<Camera>();
		}
        menu = GameObject.Find("Menu");
        playerSelect = GameObject.Find("PlayerSelect");
        diffSelect = GameObject.Find("DifficultySelect");
        overlay = GameObject.Find("Overlay");
        score = GameObject.Find("Score");
        paused = GameObject.Find("Paused");
        pButton = overlay.GetComponentInChildren<Button>();
        gameOver = GameObject.Find("GameOver");
        menu.SetActive(true);
        overlay.SetActive(false);
        paused.SetActive(false);
        gameOver.SetActive(false);
        GameObject.Find("PlayerSelect").SetActive(true);
        GameObject.Find("DifficultySelect").SetActive(false);
        pauseGame = true;
        menuAudio = gameObject.AddComponent<AudioSource>();
        menuSound = (AudioClip)Resources.Load("menuClick");
        gameStartSound = (AudioClip)Resources.Load("gameStart");
        gameOverSound = (AudioClip)Resources.Load("gameOver");
    }

    void Update()
	{
        Text[] values = score.GetComponentsInChildren<Text>();
        values[0].text = cpuScore.ToString();
        values[1].text = playerScore.ToString();

        if (!pauseGame && (cpuScore > scoreToWin || playerScore > scoreToWin))
        {
            GameOver();
            return;
        }

        if (!twoPlayerMode)
        {
            if (!pauseGame && mainCamera != null && Input.GetMouseButton(0))
            {
                Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit[] hits = Physics.RaycastAll(ray);
                for (int i = 0; i < hits.Length; i++)
                {
                    if (hits[i].transform.name == "Surface")
                    {
                        if (hits[i].point.z < 0)
                        {
                            lastMousePosition = hits[i].point;
                        }
                    }
                }
            }
            return;
        }

        var tapCount = Input.touchCount;
        if (!pauseGame && mainCamera != null && tapCount > 0)
        {
            for (var i = 0; i < tapCount; i++)
            {
                var touch = Input.GetTouch(i);
                if (touch.phase == TouchPhase.Began)
                {
                    if (touch.position.y < Screen.height / 2)
                    {
                        p1Touch = touch.fingerId;
                    }
                    else if (touch.position.y > Screen.height / 2)
                    {
                        p2Touch = touch.fingerId;
                    }
                }
            }

            bool p1Touching = false;
            bool p2Touching = false;
            for (var i = 0; i < tapCount; i++)
            {
                var touch = Input.GetTouch(i);
                if (p1Touch == touch.fingerId) { p1Touching = true; }
                if (p2Touch == touch.fingerId) { p2Touching = true; }
            }
            if (!p1Touching)
            {
                p1Touch = -1;
            }
            if (!p2Touching)
            {
                p2Touch = -1;
            }

            if (p1Touch != -1)
            {
                Touch touch;
                if (p2Touch == -1) { touch = Input.GetTouch(0); }
                else { touch = Input.GetTouch(p1Touch); }
                Ray ray = mainCamera.ScreenPointToRay(touch.position);
                RaycastHit[] hits = Physics.RaycastAll(ray);
                for (int i = 0; i < hits.Length; i++)
                {
                    if (hits[i].transform.name == "Surface")
                    {
                        if (hits[i].point.z > 0)
                        {
                            p2LastMousePosition = hits[i].point;
                        }
                        else if (hits[i].point.z < 0)
                        {
                            lastMousePosition = hits[i].point;
                        }
                    }
                }
            }
            if (p2Touch != -1)
            {
                Touch touch;
                if (p1Touch == -1) { touch = Input.GetTouch(0); }
                else { touch = Input.GetTouch(p2Touch); }
                Ray ray = mainCamera.ScreenPointToRay(touch.position);
                RaycastHit[] hits = Physics.RaycastAll(ray);
                for (int i = 0; i < hits.Length; i++)
                {
                    if (hits[i].transform.name == "Surface")
                    {
                        if (hits[i].point.z > 0)
                        {
                            p2LastMousePosition = hits[i].point;
                        }
                        else if (hits[i].point.z < 0)
                        {
                            lastMousePosition = hits[i].point;
                        }
                    }
                }
            }
        }        
    }
    
    private void StartGame()
    {
        cpuScore = 0;
        playerScore = 0;
        menu.SetActive(false);
        overlay.SetActive(true);
        paused.SetActive(false);
        gameOver.SetActive(false);
        pauseGame = false;
        pButton.interactable = true;
        Time.timeScale = 1f;
    }
        
    public void GameOver()
    {
        menuAudio.PlayOneShot(gameOverSound);
        PuckControl.resetFlag = true;
        PlayerControl.resetFlag = true;
        CPUControl.resetFlag = true;
        SecondPlayerControl.resetFlag = true;
        lastMousePosition = new Vector3(0, 2.58f, -2.751f);
        p2LastMousePosition = new Vector3(0, 2.58f, 2.751f);
        menu.SetActive(false);
        overlay.SetActive(true);
        paused.SetActive(false);
        gameOver.SetActive(true);
        pauseGame = true;
        pButton.interactable = false;
        Text title = gameOver.GetComponentInChildren<Text>();
        if (twoPlayerMode)
        {
            if (playerScore > scoreToWin) { title.text = "Player 1 Wins!"; }
            else { title.text = "Player 2 Wins!"; }
        }
        else
        {
            if (playerScore > scoreToWin) { title.text = "You Win!"; }
            else { title.text = "You Lose"; }
        }
    }

    static public void ResetPositions()
    {
        PuckControl.resetFlag = true;
        PlayerControl.resetFlag = true;
        CPUControl.resetFlag = true;
        SecondPlayerControl.resetFlag = true;
        lastMousePosition = new Vector3(0, 2.58f, -2.751f);
        p2LastMousePosition = new Vector3(0, 2.58f, 2.751f);
    }

    public void SinglePlayerButton()
    {
        menuAudio.PlayOneShot(menuSound);
        twoPlayerMode = false;
        GameObject mallet = GameObject.Find("CPUMallet");
        if (mallet.GetComponent("SecondPlayerControl"))
        {
            Destroy(mallet.GetComponent("SecondPlayerControl"));
            mallet.AddComponent<CPUControl>();
        }
        playerSelect.SetActive(false);
        diffSelect.SetActive(true);
    }

    public void TwoPlayerButton()
    {
        menuAudio.PlayOneShot(gameStartSound, 0.25f);
        twoPlayerMode = true;
        GameObject mallet = GameObject.Find("CPUMallet");
        if (mallet.GetComponent("CPUControl"))
        {
            Destroy(mallet.GetComponent("CPUControl"));
            mallet.AddComponent<SecondPlayerControl>();
        }
        StartGame();
    }

    public void PauseButton()
    {
        menuAudio.PlayOneShot(menuSound);
        if (!pauseGame)
        {
            menu.SetActive(false);
            overlay.SetActive(true);
            paused.SetActive(true);
            pauseGame = true;
            pButton.interactable = false;
            Time.timeScale = 0;
        }
        else
        {
            menu.SetActive(false);
            overlay.SetActive(true);
            paused.SetActive(false);
            pauseGame = false;
            pButton.interactable = true;
            Time.timeScale = 1f;
        }
    }

    public void QuitButton()
    {
        ResetButton();
        menu.SetActive(true);
        overlay.SetActive(false);
        paused.SetActive(false);
        gameOver.SetActive(false);
        playerSelect.SetActive(true);
        diffSelect.SetActive(false);
        pauseGame = true;
        Time.timeScale = 0f;
    }

    public void ContinueButton()
    {
        menuAudio.PlayOneShot(menuSound);
        menu.SetActive(true);
        overlay.SetActive(false);
        paused.SetActive(false);
        gameOver.SetActive(false);
        pauseGame = true;
        Time.timeScale = 0f;
    }

    public void ResetButton()
    {
        menuAudio.PlayOneShot(menuSound);
        PuckControl.resetFlag = true;
        PlayerControl.resetFlag = true;
        CPUControl.resetFlag = true;
        SecondPlayerControl.resetFlag = true;
        lastMousePosition = new Vector3(0, 2.58f, -2.751f);
        p2LastMousePosition = new Vector3(0, 2.58f, 2.751f);
        menu.SetActive(false);
        overlay.SetActive(true);
        paused.SetActive(false);
        gameOver.SetActive(false);
        pauseGame = false;
        pButton.interactable = true;
        Time.timeScale = 1f;
    }

    public void EasyButton()
    {
        menuAudio.PlayOneShot(gameStartSound, 0.25f);
        CPUControl.ChangeDifficulty(0);
        StartGame();
    }

    public void NormalButton()
    {
        menuAudio.PlayOneShot(gameStartSound, 0.25f);
        CPUControl.ChangeDifficulty(1);
        StartGame();
    }

    public void HardButton()
    {
        menuAudio.PlayOneShot(gameStartSound, 0.25f);
        CPUControl.ChangeDifficulty(2);
        StartGame();
    }

    public void BackButton()
    {
        menuAudio.PlayOneShot(menuSound);
        playerSelect.SetActive(true);
        diffSelect.SetActive(false);
    }
}