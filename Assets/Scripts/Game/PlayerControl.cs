﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
	static public bool resetFlag = false;
    Vector3 playerMalletPos;
    static public Rigidbody rb;

	void Start()
	{
		rb = GetComponent<Rigidbody> ();
	}

	void Update ()
	{
        if (MenuControls.pauseGame) { return; }

		if (resetFlag)
		{
			rb.position = new Vector3 (0, 2.58f, -2.751f);
			resetFlag = false;
			return;
		}

        Vector3 delta = Vector3.Lerp (rb.position, MenuControls.lastMousePosition, 0.35f);
		rb.MovePosition(new Vector3 (delta.x, rb.position.y, delta.z));

		playerMalletPos = rb.transform.position;
		if (playerMalletPos.z > -0.217f) { rb.transform.position
			= new Vector3 (playerMalletPos.x, playerMalletPos.y, -0.217f); }
		else if (playerMalletPos.z < -3.331f) { rb.transform.position 
			= new Vector3 (playerMalletPos.x, playerMalletPos.y, -3.331f); }
		
		playerMalletPos = rb.transform.position;
		if (playerMalletPos.x < -1.511f) { rb.transform.position 
			= new Vector3 (-1.511f, playerMalletPos.y, playerMalletPos.z); }
		else if (playerMalletPos.x > 1.511f) { rb.transform.position 
			= new Vector3 (1.511f, playerMalletPos.y, playerMalletPos.z); }
	}
}
