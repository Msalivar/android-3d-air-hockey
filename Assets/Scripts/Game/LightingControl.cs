﻿using UnityEngine;
using System.Collections;

public class LightingControl : MonoBehaviour {
	
	Light spotLight;
	float maxLight = 2.3f;
	float lightGain = 0.05f;

	// Use this for initialization
	void Start ()
	{
		spotLight = GetComponent<Light> ();
		spotLight.intensity = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (spotLight.intensity < maxLight)
		{
			spotLight.intensity += lightGain;
		}
	}
}
