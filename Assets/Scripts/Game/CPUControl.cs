﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CPUControl : MonoBehaviour
{
	static public Vector3 cpuMalletPos;
	static public bool resetFlag = false;
    static float chaseSpeed = 4f;
    static float homeSpeed = 2f;
	Rigidbody rb;
    Transform tf;
    int timer = 0;
    static public int collisionCount = 0;

	void Start ()
	{
		rb = GetComponent<Rigidbody> ();
        tf = GetComponent<Transform> ();
	}

	// Update is called once per frame
	void Update ()
    {
        if (MenuControls.pauseGame) { return; }

        if (resetFlag)
		{
			rb.MovePosition(new Vector3 (0, 2.58f, 2.751f));
			resetFlag = false;
            collisionCount = 0;
			return;
		}

        if (PuckControl.rb.position.z > -0.4f)
        {
            if (timer > 0)
            {
                timer--;
                if (timer < 1) { collisionCount = 0; }
                MoveAwayFromPuck();
                return;
            }
            else if (collisionCount > 1)
            {
                timer = 8;
                MoveAwayFromPuck();
                return;
            }
            MoveTowardPuck();
        }
        else
        {
            MoveHome();
        }
    }

    static public void ChangeDifficulty(int index)
    {
        switch(index)
        {
            case 0:
                chaseSpeed = 3f;
                homeSpeed = 2f;
                break;
            case 1:
                chaseSpeed = 4f;
                homeSpeed = 2f;
                break;
            case 2:
                chaseSpeed = 6f;
                homeSpeed = 3f;
                break;
            default:
                chaseSpeed = 4f;
                homeSpeed = 2f;
                break;
        }
    }

    void MoveTowardPuck()
    {
        //Vector3 puckDelta = Vector3.Lerp(rb.position, PuckControl.rb.position, Time.deltaTime);
        //rb.MovePosition(new Vector3(puckDelta.x, rb.position.y, puckDelta.z));
        Vector3 direction = (PuckControl.tf.position - tf.position).normalized;
        rb.MovePosition(transform.position + direction * chaseSpeed * Time.deltaTime);
    }

    void MoveHome()
    {
        //Vector3 homeDir = new Vector3(PuckControl.rb.position.x, 2.58f, 2.751f);
        //Vector3 homeDelta = Vector3.Lerp(rb.position, homeDir, Time.deltaTime);
        //rb.MovePosition(new Vector3(homeDelta.x, rb.position.y, homeDelta.z));
        Vector3 home = rb.position;
        if (!IsWithinRange(PuckControl.rb.position.x, rb.position.x))
        {
            home.x = PuckControl.rb.position.x;
        }
        if (!IsWithinRange(2.751f, rb.position.z))
        {
            home.z = 2.751f;
        }
        Vector3 direction = (home - tf.position).normalized;
        rb.MovePosition(transform.position + direction * homeSpeed * Time.deltaTime);
    }

    void MoveAwayFromPuck()
    {
        Vector3 direction = (tf.position - PuckControl.tf.position).normalized;
        rb.MovePosition(transform.position + direction * homeSpeed * Time.deltaTime);
    }

    bool IsWithinRange(float puck, float cpu)
    {
        if (Mathf.Abs(puck - cpu) < 0.06f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
