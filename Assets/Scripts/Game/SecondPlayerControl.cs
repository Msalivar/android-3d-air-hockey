﻿using UnityEngine;
using System.Collections;

public class SecondPlayerControl : MonoBehaviour
{
    static public bool resetFlag = false;
    Vector3 player2MalletPos;
    static public Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (MenuControls.pauseGame) { return; }

        if (resetFlag)
        {
            rb.position = new Vector3(0, 2.58f, 2.751f);
            resetFlag = false;
            return;
        }

        Vector3 delta = Vector3.Lerp(rb.position, MenuControls.p2LastMousePosition, 0.35f);
        rb.MovePosition(new Vector3(delta.x, rb.position.y, delta.z));

        player2MalletPos = rb.transform.position;
        if (player2MalletPos.z < 0.217f) { rb.transform.position
                = new Vector3(player2MalletPos.x, player2MalletPos.y, 0.217f); }
        else if (player2MalletPos.z > 3.331f) { rb.transform.position
                = new Vector3(player2MalletPos.x, player2MalletPos.y, 3.331f); }

        player2MalletPos = rb.transform.position;
        if (player2MalletPos.x < -1.511f) { rb.transform.position
                = new Vector3(-1.511f, player2MalletPos.y, player2MalletPos.z); }
        else if (player2MalletPos.x > 1.511f) { rb.transform.position
                = new Vector3(1.511f, player2MalletPos.y, player2MalletPos.z); }
    }
}
