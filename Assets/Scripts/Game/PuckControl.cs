﻿using UnityEngine;
using System.Collections;

public class PuckControl : MonoBehaviour
{
	static public bool resetFlag = false;
	static public Rigidbody rb;
    static public Transform tf;
    AudioSource puckAudio;
    AudioClip wallSound;
    AudioClip malletSound;

    void Start ()
	{
		rb = GetComponent<Rigidbody>();
        tf = GetComponent<Transform>();
        puckAudio = gameObject.AddComponent<AudioSource>();
        wallSound = (AudioClip)Resources.Load("puckHit");
        malletSound = (AudioClip)Resources.Load("malletHit");
    }

	// Update is called once per frame
	void Update ()
	{
        if (MenuControls.pauseGame)
        {
            return;
        }

        if (resetFlag)
		{
            if (MenuControls.last == MenuControls.lastScore.cpu)
            {
                rb.MovePosition(new Vector3(0, 2.6f, -0.886f));
            }
            else
            {
                rb.MovePosition(new Vector3(0, 2.6f, 0.886f));
            }
			rb.velocity = Vector3.zero;
			resetFlag = false;
			return;
		}
	}

	void OnCollisionEnter(Collision hit)
    {
        if (hit.gameObject.name == "PlayerMallet")
        {
            puckAudio.PlayOneShot(malletSound);
            rb.AddForce(hit.contacts[0].normal/4, ForceMode.Impulse);
        }
        if (hit.gameObject.name == "CPUMallet")
        {
            puckAudio.PlayOneShot(malletSound);
            rb.AddForce(hit.contacts[0].normal/4, ForceMode.Impulse);
            CPUControl.collisionCount++;
        }
        if (hit.gameObject.name != "PlayerMallet" && hit.gameObject.name != "CPUMallet")
        {
            puckAudio.PlayOneShot(wallSound, 0.35f);
        }
    }

	void OnCollisionStay(Collision hit)
	{
		if (hit.gameObject.name == "PlayerMallet")
		{
            if (Vector3.Distance(rb.position, PlayerControl.rb.position) < 0.335f)
            {
                Vector3 dir = rb.position - PlayerControl.rb.position;
                rb.MovePosition(rb.position + dir/4);
            }
            rb.AddForce(hit.contacts[0].normal/4, ForceMode.Impulse);
        }
        if (hit.gameObject.name == "CPUMallet")
        {
            rb.AddForce(hit.contacts[0].normal/4, ForceMode.Impulse);
            CPUControl.collisionCount++;
        }
	}

    void OnCollisionExit(Collision hit)
    {
        //if (hit.gameObject.name == "PlayerMallet")
        //{

        //}
        //if (hit.gameObject.name == "CPUMallet")
        //{
            
        //}
    }
}
