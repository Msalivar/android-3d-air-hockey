﻿using UnityEngine;
using System.Collections;

public class SlotControl : MonoBehaviour
{

    AudioSource slotAudio;
    AudioClip goalSound;

    void Start()
    {
        slotAudio = gameObject.AddComponent<AudioSource>();
        goalSound = (AudioClip)Resources.Load("score");
    }

	void OnTriggerEnter(Collider other)
    {
        slotAudio.PlayOneShot(goalSound);
        if (name == "Slot A Trigger" && other.name == "Puck")
		{
            MenuControls.playerScore++;
			MenuControls.ResetPositions ();
            MenuControls.last = MenuControls.lastScore.player;
        }
		else if (name == "Slot B Trigger" && other.name == "Puck")
		{
            MenuControls.cpuScore++;
            MenuControls.ResetPositions ();
            MenuControls.last = MenuControls.lastScore.cpu;
        }
	}
}